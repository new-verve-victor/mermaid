// eslint-disable-next-line no-unused-vars
import ForgeUI, {
  render, Fragment, Macro, Text, ConfigForm, TextArea, Select,
  useConfig, Image, Option, Button, useState, ButtonSet,
} from '@forge/ui';
import { Base64 } from 'js-base64';

// Mermaid server url for rendering SVGs
const mermaidUrl = 'https://mermaid.ink/svg';

// Templates
const templates = {
  flow: `graph TD
    A[Christmas] -->|Get money| B(Go shopping)
    B --> C{Let me think}
    C -->|One| D[Laptop]
    C -->|Two| E[iPhone]
    C -->|Three| F[fa:fa-car Car]`,
  sequence: `sequenceDiagram
    Alice->>+John: Hello John, how are you?
    Alice->>+John: John, can you hear me?
    John-->>-Alice: Hi Alice, I can hear you!
    John-->>-Alice: I feel great!`,
  class: `classDiagram
    Animal <|-- Duck
    Animal <|-- Fish
    Animal <|-- Zebra
    Animal : +int age
    Animal : +String gender
    Animal: +isMammal()
    Animal: +mate()
    class Duck{
      +String beakColor
      +swim()
      +quack()
    }
    class Fish{
      -int sizeInFeet
      -canEat()
    }
    class Zebra{
      +bool is_wild
      +run()
    }`,
  state: `stateDiagram
    [*] --> Still
    Still --> [*]

    Still --> Moving
    Moving --> Still
    Moving --> Crash
    Crash --> [*]`,
  gantt: `gantt
    title A Gantt Diagram
    dateFormat  YYYY-MM-DD
    section Section
    A task           :a1, 2014-01-01, 30d
    Another task     :after a1  , 20d
    section Another
    Task in sec      :2014-01-12  , 12d
    another task     :24d`,
  pie: `pie title Pets adopted by volunteers
    "Dogs" : 386
    "Cats" : 85
    "Rats" : 15`,
  er: `erDiagram
    CUSTOMER }|..|{ DELIVERY-ADDRESS : has
    CUSTOMER ||--o{ ORDER : places
    CUSTOMER ||--o{ INVOICE : "liable for"
    DELIVERY-ADDRESS ||--o{ ORDER : receives
    INVOICE ||--|{ ORDER : covers
    ORDER ||--|{ ORDER-ITEM : includes
    PRODUCT-CATEGORY ||--|{ PRODUCT : contains
    PRODUCT ||--o{ ORDER-ITEM : "ordered in"`,
};

const encode = (code, mermaid) => (
  encodeURIComponent(
    Base64.encode(
      JSON.stringify({ code, mermaid }),
    ),
  )
);

const Config = () => {
  const config = useConfig();
  const [defaultMarkdown, setDefaultMarkdown] = useState(null);

  return (
    <Fragment>
      <Text>
        **Write [Mermaid](https://mermaid-js.github.io/mermaid) markdown to generate diagrams and charts.**
        {'\n'}
        See [examples here](https://mermaid-js.github.io/mermaid/#/examples)!
        Test your diagrams and charts with the [live editor](https://mermaid-js.github.io/mermaid-live-editor).
        {'\n'}
        Reference:
        [Flow Chart](https://mermaid-js.github.io/mermaid/#/flowchart),
        [Sequence Diagram](https://mermaid-js.github.io/mermaid/#/sequenceDiagram),
        [Class Diagram](https://mermaid-js.github.io/mermaid/#/classDiagram),
        [State Diagram](https://mermaid-js.github.io/mermaid/#/stateDiagram),
        [Entity Relationship Diagram](https://mermaid-js.github.io/mermaid/#/entityRelationshipDiagram),
        [User Journey](https://mermaid-js.github.io/mermaid/#/user-journey),
        [Gantt Chart](https://mermaid-js.github.io/mermaid/#/gantt),
        [Pie Chart](https://mermaid-js.github.io/mermaid/#/pie)
      </Text>
      {!config && (
        <Fragment>
          <Text>
            **Load sample diagram:**
          </Text>
          <ButtonSet>
            <Button text="Flow Chart" onClick={() => setDefaultMarkdown(templates.flow)} />
            <Button text="Sequence Diagram" onClick={() => setDefaultMarkdown(templates.sequence)} />
            <Button text="Class Diagram" onClick={() => setDefaultMarkdown(templates.class)} />
            <Button text="State Diagram" onClick={() => setDefaultMarkdown(templates.state)} />
            <Button text="Gantt Chart" onClick={() => setDefaultMarkdown(templates.gantt)} />
            <Button text="Pie Chart" onClick={() => setDefaultMarkdown(templates.pie)} />
            <Button text="Entity Relationship Diagram" onClick={() => setDefaultMarkdown(templates.er)} />
          </ButtonSet>
        </Fragment>
      )}
      <ConfigForm>
        <TextArea label="Markdown" name="markdown" defaultValue={defaultMarkdown} isMonospaced />
        <Select label="Theme" name="theme">
          <Option defaultSelected label="Default" value="default" />
          <Option label="Forest" value="forest" />
          <Option label="Dark" value="dark" />
          <Option label="Neutral" value="neutral" />
        </Select>
      </ConfigForm>
    </Fragment>
  );
};

const App = () => {
  const config = useConfig();

  const mermaid = config && {
    theme: config.theme || 'default',
  };

  const url = config && config.markdown && `${mermaidUrl}/${encode(config.markdown, mermaid)}`;

  return (
    <Fragment>
      {url && <Image src={url} />}
      {!url && <Text>Configure to start drawing 🖊</Text>}
    </Fragment>
  );
};

// eslint-disable-next-line import/prefer-default-export
export const run = render(
  <Macro
    app={<App />}
    config={<Config />}
  />,
);
